$("body").on("click", ".carousel-item", function(e) {
    e.preventDefault();
    $(".listing-item-container").each(function() {
        $(this).css({ "border-style": "hidden", "border-color": "white" });
    });
    $(this).children(".listing-item-container").css({ "border-style": "solid", "border-color": "#1893d6" });
    $("#opcionParque-cart").html($(this).children(".listing-item-container").children('.listing-item').children('.listing-item-content').children('.name-option').text() + "&nbsp;");
    $("#adultos-cart").html($('#adultos').val() + "&nbsp;");
    $("#niños-cart").html($('#niños').val());
    $("#dias-cart").html($('#dias-search').val());
    $("#precioTotal-cart").html();
    $("#fecha-cart").html($('#fecha-option').text());
    if ($('#footer-cart').css('display') == 'none') {
        $("#footer-cart").css({ 'opacity': 0 });
        $("#footer-cart").animate({ 'opacity': 1 }, 500);
    }
    $("#footer-cart").css("display", "none");
});


$("body").on("click", "#confirm_cart", function(e) {
    var fecha = $('#fecha-option').text();
    $(this).val(function() {
        option = $(this).attr("data-option");
        return $(this).attr("data-option");
    });
    Swal.fire({
        title: "<h2 style='padding-top:30px; font-size:48px; margin-bottom:0px; color:#595959;'>¡Antes de continuar!",
        // html: "<span>Antes de continuar necesitamos confirmar si le diste un vistazo a la disponibilidad en la pagina de <strong>Disney</strong> y que es posible asistir en las fechas que especificaste<br/>" + "<strong>" + fecha + "</strong>" + "<br/><br/>Puedes revisarlo dando <br/><a style='color: #005eb8' target='blank_' href='https://disneyworld.disney.go.com/availability-calendar/?segments=tickets,resort,passholder&defaultSegment=tickets'>Click aquí</a></span>",
        html: "<div style='padding-top:10px'><p style='font-size:20px; line-height:36px; text-align:left;color:#222222; padding-bottom: 18px;'><strong>1.</strong>. Pensando en su seguridad, los parques han establecido un límite de disponibilidad para poder cumplir con las medidas de distanciamiento establecidas.<br> <strong>2.</strong>. <strong>Es indispensable consultar la disponibilidad</strong> del parque seleccionado en esta sección haga <a style='color: #005eb8' target='blank_' href='https://disneyworld.disney.go.com/availability-calendar/?segments=tickets,resort,passholder&defaultSegment=tickets'>Click aquí</a>.<br> <strong>3.</strong> Después de la compra debe hace el registro en <strong>My Disney Experience</strong>, reservar la entrada los días que va a asistir a cada parque dependiendo del tipo de entrada y ¡a disfrutar de la diversión!<br><strong>4.</strong> Si requiere más información contáctenos por el chat y/o link del asesor en línea uno de nuestros agentes le asesorará.<br><a class='hipervinculo-href'href='https://www.aviatur.com/contenidos/asesor-en-linea' target='_blank' style='color:#005EB8;'>https://www.aviatur.com/contenidos/asesor-en-linea</a></p> <div style='border-top:2px solid #C4C4C4; padding-top:35px;'><p style='text-align:left; weight:600; font-size:20px; line-height:36px;'>Confirme que le dio un vistazo a la disponibilidad en la página de Disney y que le es posible asistir en las fechas que especificó: <strong>" + fecha + "</strong></br></br>Puede revisarlo <a class='hipervinculo-href'href='https://disneyworld.disney.go.com/availability-calendar/?segments=tickets,resort,passholder&defaultSegment=tickets' target='_blank' style='color:#005EB8;'>aquí</a></p></div>",
        customClass: {
            cancelButton: 'swal2-cancel',
        },
        showCancelButton: true,
        // cancelButtonColor: '#6e7d88',   
        confirmButtonColor: '#00c382',
        confirmButtonText: 'Si, ¡Confirmo!',
        cancelButtonText: 'Cancelar',
        focusConfirm: false,
        width: '85vw',
        height: '1000px'
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire({
                title: '¡Perfecto!',
                icon: 'success',
                html: 'Continuemos con tu compra',
                timer: 2500,
                timerProgressBar: true,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading();
                }
            }).then((result) => {
                $("#adultos-invoice").val($("#adultos").val());
                $("#niños-invoice").val($("#niños").val());
                $("#precioAdultos-invoice").val($("#total-adultos").text());
                $("#precioNiños-invoice").val($("#total-niños").text());
                $("#precioTotal-invoice").val($("#precioTotal-cart-" + option).text());
                $("#fecha-invoice").val($("#booking-date-search").val());
                $("#dias-invoice").val($("#dias-search").val());
                $("#opcion-invoice").val($("#opcionParque-cart").text());
                if (result.dismiss === Swal.DismissReason.timer) {
                    $("#shopping-cart").submit();
                }
            })
        }
    })
});

function ocultarFooter() {
    var x = document.getElementById("footer-cart");
    x.style.display = "none";
}