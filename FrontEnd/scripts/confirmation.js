$("body").on("click", "#confirm_and_pay", function (e) {
    var fecha = $('#fecha').text()
    fecha = new Date(fecha).toLocaleDateString('en-CA');
    var dias = $('#total-dias').text();
    var total = $('#total-precio').text();
    var total = parseInt(total);

    var pesosCol = $('#total-pesos').text();
    var niños = $('#cantidad-niños').text();
    var formateado = total;
    var adultos = $('#cantidad-adultos').text();

    var descripcion = parseInt(niños) + parseInt(adultos);
    
    /* Datos para facturar */
    var nameinvoice = $("#nameOnCard").val();
    var apellidoinvoice = $("#apellidoOnCard").val();
    var typedocument = $("#typeDoc").val();
    var numberdocument = $("#numberDocument").val();
    var emailinvoice = $("#emailInvoice").val();
    var phoneinvoice = $("#phoneInvoice").val();
    var num_tarjeta = $('#cardNumber').val();
    var expCard = $('#expiryDate').val() + "20" + $('#expirynDate').val();
    
    var expirationMonth = $('#expiryDate').val();
    var expirationYear = $('#expirynDate').val();
    
    
    
    var codCard = $('#cvv').val();
    var pasajeros_nombre = $('input[name*="data_nombre[]"]');
    var sendDescripcion = "ENTRADAS PAQUETES DISNEY POR " + dias + " DÍAS PARA PASAJEROS " ;
    var pasajeros_apellido = $('input[name*="data_apellido[]"]');
    var pasajeros_nacionalidad = $('input[name*="data_nacionalidad[]"]');
    var pasajeros_numero_identificacion = $('input[name*="data_no_identificacion[]"]');
    var pasajeros_correo_electronico = $('input[name*="data_correo[]"]');
    var pasajeros_telefono = $('input[name*="data_tel[]"]');
    var pasajeros_genero = $('select[name*="data_genero[]"]');
    var pasajeros_tipo_identificacion = $('select[name*="data_identificacion[]"]');
    var pasajeros_fecha_nacimiento = $('input[name*="data_fecha[]"]');

    var nombres_array = [];
    var apellidos_array = [];
    var nacionalidad_array = [];
    var numero_identificacion_array = [];
    var correo_array = [];
    var telefono_array = [];
    var genero_array = [];
    var tipo_identificacion_array = [];
    var fecha_nacimiento_array = [];

    //var num_tarjeta_crip = CryptoJS.enc.Base64(num_tarjeta);
    var error = "";
    var expirationYearComparison = "20" + $('#expirynDate').val();
    var expirationYearComparisonInt = parseInt(expirationYearComparison);
    var actual_year = new Date().getFullYear();

    function YearComparison() {
        if (expirationYearComparisonInt >= actual_year) {
            return true;
        } else {
            return false;
        }
    };
    var nombres_full = '';
    console.log();

    for (var i = 0; i < (parseInt(adultos) + parseInt(niños)); i++) {

        pasajero = i + 1;

        nombres_array[i] = pasajeros_nombre[i].value;
        nombres_full = nombres_full + pasajeros_nombre[i].value;
        if (nombres_array[i] == "") {
            error = 'Por favor complete el campo correspondiente al <strong>nombre</strong> del pasajero ' + pasajero;
            break;
        }
        nombres_full = nombres_full + ' ';
        nombres_full = nombres_full + pasajeros_apellido[i].value;
        apellidos_array[i] = pasajeros_apellido[i].value;
        if (apellidos_array[i] == "") {
            error = 'Por favor complete el campo correspondiente al <strong>apellido</strong> del pasajero ' + pasajero;
            break;
        }
        nombres_full = nombres_full + ',';
        genero_array[i] = pasajeros_genero[i].value;
        if (genero_array[i] == 'Femenino' || genero_array[i] == 'Masculino') {
            error = '';
        } else {
            error = 'Por favor asigne el <strong>género</strong> del pasajero' + pasajero;
            break;
        }
        nacionalidad_array[i] = pasajeros_nacionalidad[i].value;
        if (nacionalidad_array[i] == "") {
            error = 'Por favor complete el campo correspondiente a la <strong>nacionalidad</strong> del pasajero ' + pasajero;
            break;
        }
        tipo_identificacion_array[i] = pasajeros_tipo_identificacion[i].value;
        if (tipo_identificacion_array[i] == "") {
            error = 'Por favor escoja el <strong>tipo de identificación</strong> del pasajero ' + pasajero;
            break;
        }
        numero_identificacion_array[i] = pasajeros_numero_identificacion[i].value;
        if (numero_identificacion_array[i] == "") {
            error = 'Por favor complete el campo correspondiente a el <strong>número de identificación</strong> del pasajero ' + pasajero;
            break;
        }
        fecha_nacimiento_array[i] = pasajeros_fecha_nacimiento[i].value;
        if (fecha_nacimiento_array[i] == "") {
            error = 'Por favor seleccione la <strong>fecha de nacimiento</strong> del pasajero ' + pasajero;
            break;
        }
        correo_array[i] = pasajeros_correo_electronico[i].value;
        if (correo_array[i] == "") {
            error = 'Por favor asigne un <strong>correo electrónico</strong> para el pasajero ' + pasajero + ' en el campo correspondiente';
            break;
        }
        telefono_array[i] = pasajeros_telefono[i].value;
        if (telefono_array[i] == "") {
            error = 'Por favor asigne un <strong>número de teléfono</strong> para el pasajero' + pasajero;
            break;
        }
        //falta validar el en medio de pago y que no sean requeridos cuando el medio de pago es diferente a tarjeta de credito a debito
        if (nameinvoice == "") {
            error = 'Por favor complete el campo correspodiente a <strong>nombre en tarjeta</strong> en medio de pago';
            break;
        }
        if (apellidoinvoice == "") {
            error = 'Por favor complete el campo correspodiente a <strong>apellido en tarjeta</strong> en medio de pago';
            break;
        }
        if (phoneinvoice == "") {
            error = 'Por favor complete el campo correspodiente a <strong>número de teléfono</strong> en medio de pago';
            break;
        }
        if (typedocument == 00) {
            error = 'Por favor seleccione el <strong>tipo de documento</strong> en medio de pago';
            break;
        }
        if (numberdocument == "") {
            error = 'Por favor complete el campo correspodiente a <strong>número de documento</strong> en medio de pago';
            break;
        }
        if (emailinvoice == "") {
            error = 'Por favor complete el campo correspodiente a <strong>correo electrónico</strong> en medio de pago';
            break;
        }
        if (num_tarjeta == "") {
            error = 'Por favor verifique el <strong>número de su tarjeta</strong> en el medio de pago';
            break;
        }
        if (expirationMonth == 00) {
            error = 'Por favor complete el campo correspodiente al <strong>mes de expiración de su tarjeta</strong> en medio de pago';
            break;
        }
        if (expirationYear == "") {
            error = 'Por favor complete el campo correspodiente al <strong>año de expiración de su tarjeta</strong> en medio de pago';
            break;
        }
        if (YearComparison() == false) {
            error = 'Por favor complete el campo correspodiente al <strong>año de expiración de su tarjeta</strong> en medio de pago, con un año que sea válido(mayor o igual al año actual)';
            break;
        }
        if (codCard == "") {
            error = 'Por favor complete el campo correspodiente al <strong>CVV de su tarjeta</strong> en medio de pago';
            break;
        }

        if($('#condicionespol').prop('checked')==false){
            error = 'Por favor acepte las condiciones, restricciones y política de privacidad';
            break;
        }

        if($('#condicionescovid').prop('checked')==false){
            error = 'Por favor acepte las restricciones y políticas de viaje por COVID-19.';
            break;
        }
    }


    var datos_pasajeros = {
        "nombres": nombres_array,
        "apellidos": apellidos_array,
        "genero": genero_array,
        "nacionalidad": nacionalidad_array,
        "identificacion": numero_identificacion_array,
        "tipo identificacion": tipo_identificacion_array,
        "correo": correo_array,
        "telefono": telefono_array,
        "fecha nacimiento": fecha_nacimiento_array
    };
    var sendDescripcion = "ENTRADAS PAQUETES DISNEY POR " + dias + " DÍAS PARA PASAJEROS " + nombres_full;
    sendDescripcion = sendDescripcion.substring(0, sendDescripcion.length - 1);
    console.log(sendDescripcion);
    if (error != '') {
        Swal.fire({
            confirmButtonColor: '#005eb8',
            confirmButtonText: '¡Entendido!',
            icon: 'error',
            title: 'Error',
            html: error,
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
            }
        })
    } else {
        Swal.fire({
            title: "<h2 style='padding-top:30px; font-size:44px; margin-bottom:0px; color:#595959;'>Para continuar con su compra",
            // html: "<span>Antes de continuar necesitamos confirmar si le diste un vistazo a la disponibilidad en la pagina de <strong>Disney</strong> y que es posible asistir en las fechas que especificaste<br/>" + "<strong>" + fecha + "</strong>" + "<br/><br/>Puedes revisarlo dando <br/><a style='color: #005eb8' target='blank_' href='https://disneyworld.disney.go.com/availability-calendar/?segments=tickets,resort,passholder&defaultSegment=tickets'>Click aquí</a></span>",
            html: "<br/><p style='font-size:20px;'>Para continuar con su travesía a <strong>Disney</strong> es necesario confirmar los siguientes datos:<br/><br/>Número de personas: <strong>" + descripcion + "</strong><br/>Fecha: <strong>" + fecha + "</strong><br/>Número de días: <strong>" + dias + "</strong><br/></br>Precio Total: <strong> " + pesosCol + "<strong>",
            customClass: {
                cancelButton: 'swal2-cancel',
            },
            showCancelButton: true,
            // cancelButtonColor: '#6e7d88',   
            confirmButtonColor: '#00c382',
            confirmButtonText: 'Si, ¡Confirmo!',
            cancelButtonText: 'Cancelar',
            focusConfirm: false,
            width: '800px'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: '¡Estás a un paso!',
                    icon: 'success',
                    html: 'Continuemos con tu compra y nos vemos en un instante!',
                    timer: 1000,
                    timerProgressBar: true,
                    allowOutsideClick: false,
                    didOpen: () => {
                        Swal.showLoading();
                    }
                }).then((result) => {
                    $('#confirm_and_pay').html('<i class="fa fa-refresh fa-spin"></i> Cargando...');
                    $('#confirm_and_pay').attr("disabled", true);
                    $('#confirm_and_pay').css({ width: "25vw" });
                    $("#nombrefactura").val(nameinvoice);
                    $("#apellidofactura").val(apellidoinvoice);
                    $("#tipodoc").val(typedocument);
                    $("#numerodoc").val(numberdocument);
                    $("#correofactura").val(emailinvoice);
                    $("#celularfactura").val(phoneinvoice);
                    $("#fechainicial").val(fecha);
                    $("#monto").val(formateado);
                    $("#descripcion").val(sendDescripcion);
                    $("#datos_adicionales").val(JSON.stringify(datos_pasajeros));
                    $("#exp_card").val(expCard);
                    $("#dias").val(dias);
                    $("#cod_card").val(codCard);
                    $("#numero_tarjeta").val(num_tarjeta);
                    if (result.dismiss === Swal.DismissReason.timer) {
                        $("#Create-Pay").submit();
                        $("#datos-exp input").prop("disabled",true);
                        $("#datos-exp select").prop("disabled",true);
                    }
                })
            }
        })
    }

});

$("#cardNumber").keypress(function (e) {
    if ((e.which < 48 || e.which > 57) && (e.which !== 8) && (e.which !== 0)) {
        return false;
    }

    return true;
});


$('#expirynDate').keypress(function (e) {
    var actual_year = new Date().getFullYear();

    if (e.value < actual_year) {
        return false;
    }
    return true;
});



