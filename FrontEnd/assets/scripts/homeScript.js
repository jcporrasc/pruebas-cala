function pruebaClick() {

    // $.ajax({
    //     url: "http://127.0.0.1:5000/",
    //     method: "GET",
    // }).done(function (response) {
    //     console.log(response)
    // }).fail(function (error) {
    //     console.log(error);
    // });

    $('#tablaPedidos').DataTable({
        destroy: true,
        paging: true,
        searching: true,
        ajax: 'http://127.0.0.1:5000/',
        columns: [
            { data: 'id' },
            { data: 'nombre' },
            { data: 'apellido' },
            { data: 'cedula' },
            { data: 'nacimiento' },
            { data: 'nombreCompleto' },
            { data: 'edad' },
            { data: 'tipoPedido' },
            { data: 'numPedido' },
        ]
    });
}

function graficar() {
    $.ajax({
        url: "http://127.0.0.1:5000/dataGrafica",
        method: "GET",
    }).done(function (response) {
        TESTER = document.getElementById('PedidosGrafica');
        var data = [
            response
        ];
        var layout = { barmode: 'stack' };

        Plotly.newPlot('PedidosGrafica', data, layout);
        /* Current Plotly.js version */
        console.log(Plotly.BUILD);
    }).fail(function (error) {
        console.log(error);
    });


}

$.ajax({
    url: "http://127.0.0.1:5000/rutaAcceso",
    method: "GET",
}).done(function (response) {
    $('#rutaAccesoId').text('' + response);
}).fail(function (error) {
    console.log(error);
});


