from flask import Flask , render_template
from flask_cors import CORS
import mysql.connector
import json
import datetime
from datetime import date
import pandas as pd
import sqlalchemy


global dataStatic

with open('Include/json/varStatic.json') as file:
        dataStatic = json.load(file)

database =  mysql.connector.connect(
    host= dataStatic['sql']['host'],
    user= dataStatic['sql']['user'],
    database= dataStatic['sql']['database']
)

database_username = dataStatic['sql']['user']
database_ip       = dataStatic['sql']['host']
database_name     = dataStatic['sql']['database']
database_connection = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                               format(database_username, '', 
                                                      database_ip, database_name))

print(database_connection)

cursor = database.cursor()

app = Flask(__name__)
CORS(app)

@app.route('/')
def home():

    clienteData = pd.read_table( dataStatic['ruta'] + dataStatic['fileClient'], delimiter='\t')
    clienteOrders = pd.read_excel(dataStatic['ruta'] + dataStatic['fileOrders'], sheet_name='Hoja1')

    datafinal = pd.merge(clienteData, clienteOrders, left_on='CEDULA', right_on='cc_cliente')
    datafinal = datafinal.drop(datafinal.columns[[-1]], axis='columns')
    datafinal.to_sql(con=database_connection, name='clientespedidos', if_exists='replace')

    cursor.execute('SELECT * FROM pruebacala.clientespedidos')
    clientes =  cursor.fetchall()

    result = []
    for cliente in clientes:
        today = date.today()
        format_str = '%d/%m/%Y' 
        nacimiento = datetime.datetime.strptime(cliente[4], format_str)
        
        age = today.year - nacimiento.year - ((today.month, today.day) < (nacimiento.month, nacimiento.day))
        cliente = {
            'id': cliente[0],
            'nombre': cliente[1].upper(),
            'apellido': cliente[2].upper(),
            'cedula': cliente[3],
            'nacimiento': nacimiento.strftime("%d/%m/%Y"),
            'nombreCompleto': cliente[1].capitalize() + ' ' + cliente[2].capitalize(),
            'edad': age,
            'tipoPedido': cliente[7],
            'numPedido': cliente[6],
        }
        result.append(dict(cliente))
    return { 'data': result}

@app.route('/rutaAcceso')
def rutaAcceso():
        result = dataStatic['ruta']
        return result


@app.route('/dataGrafica')
def dataGrafica():
    cursor.execute('SELECT  NOMBRE, APELLIDO, COUNT("numero de pedido"), CEDULA FROM clientespedidos GROUP BY NOMBRE')
    datos =  cursor.fetchall()
    nombre = []
    cantidad = []
    for cliente in datos:
        nombre.append(cliente[0].upper() + ' ' +cliente[1].upper())
        cantidad.append(cliente[3])
        
    return {
          'x': nombre,
          'y': cantidad,
          'name' :'clientes vs pedidos',
          'type': 'bar'
        }