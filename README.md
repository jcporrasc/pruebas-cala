# Pruebas Cala



## Instructivo del aplicativo:

El aplicativo contiene 2 carpetas, una que es el Front y otra que es el Back, para la correcta ejecucion, por favor tener en cuenta que debe iniciar primero cambiando las rutas estaticas del archivo .json ubicado en:


```
BackEnd\Include\json\varStatic.json
```
Tener en cuenta que al cambiar las vaariables para la conexion con la DB, debe ser una DB de prueba sin contraseña, se aconseja crear una con xammp y ejecutarla, en caso de no correr una base de datos Mysql, el aplicativo no funcionara.

En el cual se encuentran las variables de conexion, nombre de los archivos ejemplos y ruta para encontrar los archivos.

Una vez terminado esto, por favor, ejecutar el ambiente virtual de python, ubicandose en la carpeta BackEnd, ejecutar ( para windows) el comando:

```
.\Scripts\activate 
```

Lo cual activa el ambiente virtual, despues ejecutar el comando:

```
flask run
```

Una vez ejecutado con exito este comando, dirigirse al siguiente archivo y abrirlo en algun buscador:

```
FrontEnd\Templates\index.html
```

alli ya podra utilizar el aplicativo sin ningun inconveniente.
